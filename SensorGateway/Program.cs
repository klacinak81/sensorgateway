﻿using Microsoft.Azure.Devices.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NLog.Extensions.Logging;
using Sense.RTIMU;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace SensorGateway
{
	class Program
	{
		private static Runner runner;
		private static IConfiguration config;
		private static string serverIp;
		private static string serverPort;
		private static TcpListener server;
		private static bool isNetworkEnabled;
		private static string macAddressExpression,
			ipAddressExpression,
			temperatureBlockExpression,
			temperatureExpression,
			switch1Expression,
			switch2Expression;
		private static DeviceClient azureDevice;
		private static string azureConnectionString;
		static void Main(string[] args)
		{
			var servicesProvider = BuildDi();
			runner = servicesProvider.GetRequiredService<Runner>();

			var builder = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json");

			config = new ConfigurationBuilder()
				.AddJsonFile("appsettings.json", true, true)
				.Build();

			isNetworkEnabled = true;
			runner.LogInfo("Application is starting.");
			runner.LogInfo("Current version is 0.2.");
			serverIp = config["serverIP"];
			serverPort = config["serverPort"];
			macAddressExpression = config["macAddressExpression"];
			ipAddressExpression = config["ipAddressExpression"];
			temperatureBlockExpression = config["temperatureBlockExpression"];
			temperatureExpression = config["temperatureExpression"];
			switch1Expression = config["switch1StatusExpression"];
			switch2Expression = config["switch2StatusExpression"];
			azureConnectionString = config["azureConnectionString"];
			runner.LogDebug("------ CONFIGURATION ------");
			runner.LogDebug($"Server IP: {serverIp}");
			runner.LogDebug($"Server port: {serverPort}");
			runner.LogDebug($"Connection string to Azure IoT Hub: {azureConnectionString}");
			runner.LogDebug($"MAC address expression: {macAddressExpression}");
			runner.LogDebug($"IP address expression: {ipAddressExpression}");
			runner.LogDebug($"Temperature block expression: {temperatureBlockExpression}");
			runner.LogDebug($"Temperature expression: {temperatureExpression}");
			runner.LogDebug($"Switch 1 state expression: {switch1Expression}");
			runner.LogDebug($"Switch 2 state expression: {switch2Expression}");

			runner.LogInfo("Setting up and starting network adapter.");
			if (SetupNetwork())
				runner.LogInfo("Network adapter is started successfully.");
			else
				runner.LogError("Network adapter cannot be started.");

			runner.LogInfo("Setting up Azure Device Client.");
			azureDevice = DeviceClient.CreateFromConnectionString(azureConnectionString);
			if (azureDevice == null)
				runner.LogError("Azure Device Client cannot be instantiated, terminating.");

			runner.LogInfo("Azure Device Client is setting up correctly.");

			runner.LogWarning("Press ENTER to terminate.");
			Console.ReadLine();
			runner.LogInfo("Application is shutting down, waiting for network to be stopped...");
			isNetworkEnabled = false;
			server.Stop();
			
			NLog.LogManager.Shutdown();
		}

		private static ServiceProvider BuildDi()
		{
			return new ServiceCollection()
				.AddLogging(builder =>
				{
					builder.SetMinimumLevel(LogLevel.Trace);
					builder.AddNLog(new NLogProviderOptions
					{
						CaptureMessageTemplates = true,
						CaptureMessageProperties = true
					});
				})
				.AddTransient<Runner>()
				.BuildServiceProvider();
		}

		private static bool SetupNetwork()
		{
			IPAddress ip;
			int port;
			if (!IPAddress.TryParse(serverIp, out ip))
			{
				runner.LogError($"TCP Server IP address value is not a valid IP address in config, please review value: {serverIp}");
				runner.LogError("Establish network adapter is failed, terminating...");
				return false;
			}
			if (!Int32.TryParse(serverPort, out port))
			{
				runner.LogError($"TCP Server port value is not a valid port number in config, please review value: {serverPort}");
				runner.LogError("Establish network adapter is failed, terminating...");
				return false;
			}
			server = new TcpListener(ip, port);
			server.Start();
			runner.LogInfo($"TCP/IP Server is listening on {ip.ToString()}:{port.ToString()}");
			(new Thread(new ThreadStart(Listen))).Start();
			return true;
		}

		private static void Listen()
		{
			runner.LogInfo("Starting loop and wait for incoming network packages.");
			while (isNetworkEnabled)
			{
				/// Accept incoming connection and handle it in an other thread.
				TcpClient tcpClient = server.AcceptTcpClient();
				StreamReader sr = new StreamReader(tcpClient.GetStream());
				string message = sr.ReadLine();
				runner.LogInfo($"MESSAGE ARRIVED from {((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString()} at ({DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}): {message}");
				DispatchMessage(message);
				MeasureOnSenseHat();
			}
		}

		private static void MeasureOnSenseHat()
		{
			using (var settings = RTIMUSettings.CreateDefault())
			using (var pressure = settings.CreatePressure())
			using (var humidity = settings.CreateHumidity())
			{
				runner.LogInfo("** Measurement on SenseHat");
				var pressureReadResult = pressure.Read();
				runner.LogInfo($"** Pressure: {pressureReadResult.Pressure}");
				var humidityReadResult = humidity.Read();
				runner.LogInfo($"** Humidity: {humidityReadResult.Humidity}");
				runner.LogInfo($"** Temperature: {humidityReadResult.Temperatur}");
			}
		}

		private static void DispatchMessage(string msg)
		{
			string msgMacAddress = "--unknown", msgIpAddress = "--unknown", msgTemperature = "--unknown", msgSwitch1State = "--unknown", msgSwitch2State = "--unknown";

			Match macAddressP = Regex.Match(msg, macAddressExpression);
			if (macAddressP.Success)
				msgMacAddress = macAddressP.Value;
			Match ipAddressP = Regex.Match(msg, ipAddressExpression);
			if (ipAddressP.Success)
				msgIpAddress = ipAddressP.Value;
			Match temperatureBlockP = Regex.Match(msg, temperatureBlockExpression);
			if (temperatureBlockP.Success)
			{
				Match temperatureP = Regex.Match(temperatureBlockP.Value, temperatureExpression);
				if (temperatureP.Success)
					msgTemperature = temperatureP.Value;
			}
			Match switch1P = Regex.Match(msg, switch1Expression);
			if (switch1P.Success)
				msgSwitch1State = switch1P.Value;
			Match switch2P = Regex.Match(msg, switch2Expression);
			if (switch2P.Success)
				msgSwitch2State = switch2P.Value;

			var telemetryDataPoint = new
			{
				MACAddress = msgMacAddress,
				IPAddress = msgIpAddress,
				Timestamp = DateTime.Now.ToString(),
				Temperature = msgTemperature,
				Switch1State = msgSwitch1State,
				Switch2State = msgSwitch2State
			};

			string jsonMessage = "";
			jsonMessage = JsonConvert.SerializeObject(telemetryDataPoint);

			var message = new Message(Encoding.ASCII.GetBytes(jsonMessage));
			if (msgIpAddress == "19.80.12.120")
				azureDevice.SendEventAsync(message);
			runner.LogInfo("Message is dispatched to the cloud.");
		}
	}	

	public class Runner
	{
		private readonly ILogger<Runner> _logger;

		public Runner(ILogger<Runner> logger)
		{
			_logger = logger;
		}

		public void LogDebug(string name)
		{
			_logger.LogDebug(20, ":: {Action}", name);
		}

		public void LogInfo(string name)
		{
			_logger.LogInformation(20, ":: {Action}", name);
		}

		public void LogError(string name)
		{
			_logger.LogError(20, ":: {Action}", name);
		}

		public void LogWarning(string name)
		{
			_logger.LogWarning(20, ":: {Action}", name);
		}
	}
}
